var parseString = require('xml2js').parseString;
var MongoClient = require('mongodb').MongoClient;
var nconf = require('nconf');
var path = require('path');
var fs = require('fs');
var _async = require('async');

nconf.file('../config/config.json');

MongoClient.connect(nconf.get('DATABASE_URL'), function(err, database) {
    if (err)
        console.log(err);

    else {
        console.log('Connected');
        db = database;

        bulkInsert()
    }
});

function bulkInsert() {
    var xml = fs.readFileSync('emails.xml').toString();
    var batch = db.collection(nconf.get('EMAIL_COLLECTION')).initializeUnorderedBulkOp({
        useLegacyOps: true
    });

    parseString(xml, function(err, result) {
        if (err)
            console.log(err);

        else {
            result.root.thread.forEach(function(thread) {
                thread.DOC.forEach(function(doc) {
                    var obj = {
                        threadName: thread.name[0],
                        threadId: thread.listno[0],
                        received: doc.Received[0],
                        from: doc.From[0],
                        to: doc.To[0],
                        subject: doc.Subject[0]
                    };

                    var sent = '';

                    doc.Text[0].Sent.forEach(function(_sent) {
                        sent += '<p>' + _sent._ + '</p>';
                    });

                    obj.text = sent;

                    batch.insert(obj);
                });
            });

            batch.execute(function(err, result) {
                if(err)
                    console.log(err);

                else {
                    console.log('Inserted :', result.nInserted);
                    process.exit();
                }
            });
        }
    });
}
