var parseString = require('xml2js').parseString;
var MongoClient = require('mongodb').MongoClient;
var nconf = require('nconf');
var path = require('path');
var fs = require('fs');
var _async = require('async');
var bcrypt = require('bcrypt-nodejs');

nconf.file('../config/config.json');

MongoClient.connect(nconf.get('DATABASE_URL'), function(err, database) {
    if (err)
        console.log(err);

    else {
        console.log('Connected');
        db = database;

        bulkInsert()
    }
});

function bulkInsert() {
    var batch = db.collection(nconf.get('USER_COLLECTION')).initializeUnorderedBulkOp({
        useLegacyOps: true
    });

    batch.insert({
        username : 'jpalme@dsv.su.se',
        password : bcrypt.hashSync('password')
    });

    batch.insert({
        username : 'tallen@sonic.net',
        password : bcrypt.hashSync('password')
    });

    batch.insert({
        username : 'brian@hursley.ibm.com',
        password : bcrypt.hashSync('password')
    });

    batch.insert({
        username : 'user',
        password : bcrypt.hashSync('user')
    });

    batch.execute(function(err, result) {
        if(err)
            console.log(err);

        else
            console.log('Inserted :', result.nInserted);

        process.exit();
    });
}
