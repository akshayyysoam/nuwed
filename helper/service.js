module.exports = function(db, nconf, urlExclusionList) {
    var bcrypt = require('bcrypt-nodejs');

    this.isEmail = function(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    this.prepareExclusionList = function(url) {
        nconf.get('URL_EXCLUSION_LIST').forEach(function(item) {
            urlExclusionList.add(item);
        });
    }

    this.generateHash = function(password) {
        return bcrypt.hashSync(password);
    };

    this.compare = function (password, hash) {
        return bcrypt.compareSync(password, hash);
    }

    return this;;
}
