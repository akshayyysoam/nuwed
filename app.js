var express = require('express');
var http = require('http');
var chalk = require('chalk');
var path = require('path');
var nconf = require('nconf');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var session = require('express-session');
var fs = require('fs');
var _async = require('async');
var twilio = require('twilio');

var app = express();
var service;

var httpServer = http.createServer(app);

nconf.file(path.join(__dirname, 'config', 'config.json'));

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

app.use(session({
    secret: 'gmail',
    resave: false,
    saveUninitialized: true
}));

var urlExclusionList = new Set();

console.log('Connecting to Database');

MongoClient.connect(nconf.get('DATABASE_URL'), function(err, database) {
    if (err)
        console.log(err);

    else {
        console.log('Connected');
        db = database;
        service = require('./helper/service')(db, nconf, urlExclusionList);

        service.prepareExclusionList();

        httpServer.listen(nconf.get('HTTP_PORT'), function(err) {
            console.log(err ? err : chalk.green('HTTP Server running on port', nconf.get('HTTP_PORT')));
        });
    }
});

app.use(function(req, res, next) {
    if (req.session.user || urlExclusionList.has(req.originalUrl))
        next();

    else {
        console.log('Unauthorised Url :', req.originalUrl);
        res.send(401);
    }
});

app.post('/login', function(req, res) {
    db.collection(nconf.get('USER_COLLECTION')).findOne({
        username: req.body.username
    }, function(err, user) {
        if (err) {
            console.log(err);
            res.send(500);
        } else if (!user || !service.compare(req.body.password, user.password)) {
            console.log('No user found for', req.body.username);

            setTimeout(function () {
                res.send(404);
            }, 2500);
        } else {
            setTimeout(function () {
                req.session.user = user;
                res.send(200);
            }, 2500);
        }
    });
});

app.get('/search', function(req, res) {
    db.collection(nconf.get('EMAIL_COLLECTION')).find({
        $text: {
            $search: req.query.query
        }
    }, {
        score: {
            $meta: 'textScore'
        }
    }, {
        limit: 10
    }).sort({
        score: {
            $meta: "textScore"
        }
    }).toArray(function(err, emails) {
        if (err) {
            console.log(err);
            res.send(500);
        } else {
            setTimeout(function() {
                res.send(emails);
            }, 2500);
        }
    });
});

app.get('/details', function(req, res) {
    db.collection(nconf.get('EMAIL_COLLECTION')).find({
        threadId: req.query.threadId
    }).sort({
        received: 1
    }).toArray(function(err, emails) {
        if (err) {
            console.log(err);
            res.send(500);
        } else {
            setTimeout(function() {
                res.send(emails);
            }, 2500);
        }
    });
});

app.post('/sendSms', function(req, res) {
    var client = new twilio(nconf.get('SMS').ACCOUNT_SID, nconf.get('SMS').AUTH_TOKEN);

    client.messages.create({
        body: req.body.subject,
        to: req.body.to,
        from: nconf.get('SMS').FROM_SMS
    }, function(err, message) {
        if (err) {
            console.log(err);
            res.send(500);
        } else {
            console.log('SMS Sent : ', message);
            res.send(200);
        }
    });
});

app.get('*', function(req, res) {
    res.sendfile('index.html');
});
