var app = angular.module('app');

app.filter('text', function() {
    return function(text) {
        text = text ? String(text).replace(/<[^>]+>/gm, '') : '';

        return (text.length > 40) ? text.substring(0, 40) + '...' : text ;
    }
});
