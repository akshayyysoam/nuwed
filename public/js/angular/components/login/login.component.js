var app = angular.module('app');

app.component('login', {
    templateUrl : 'js/angular/components/login/login.template.html',
    controller : loginController,
    controllerAs : 'ctrl'
});

function loginController($scope, $http, $location) {
    var vm = this;

    vm.$onInit = function () {
        $scope.user = {};
    }

    $scope.submit = function (e) {
        $scope.active = true;

        $http({
            url: '/login',
            method: "POST",
            data: e
        }).then(function(response) {
            $scope.active = false;

            $location.path('/search');
        }, function(response) {
            $scope.active = false;
            if(response.status == 404) {
                $scope.response = 'Invalid combination of username and password';
            }

            else
                console.log(response);
        });
    }
}
