var app = angular.module('app');

app.component('toolbar', {
    templateUrl : 'js/angular/components/toolbar/toolbar.template.html',
    controller : toolbarController,
    controllerAs : 'ctrl',
    bindings : {

    }
});

function toolbarController($scope, $http) {
    var vm = this;

    vm.$onInit = function () {

    }
}
