var app = angular.module('app');

app.component('search', {
    templateUrl: 'js/angular/components/search/search.template.html',
    controller: searchController,
    controllerAs: 'ctrl',
    bindings: {

    }
});

function searchController($scope, $http, $location, service) {
    var vm = this;

    vm.$onInit = function() {

    }

    $scope.search = function (query) {
        $scope.active = true;

        $http({
            url: 'search',
            method: "GET",
            params : { query : $scope.query }
        }).then(function(response) {
            $scope.active = false;

            if(response.data.length == 0) {
                $scope.response = 'No results found for ' + $scope.query;
            }

            else {
                service.storeSearchResults($scope.query, response.data);
                $location.path('/results').search('query', $scope.query);
            }

        }, function(response) {
            $scope.active = false;

            if (response.status == 401)
                $location.path('/');

            else {
                $scope.response = 'There has been an error while searching for query ' + $scope.query;
                console.log(response);
            }
        });
    }
}
