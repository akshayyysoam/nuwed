var app = angular.module('app');

app.component('detail', {
    templateUrl: 'js/angular/components/detail/detail.template.html',
    controller: detailController,
    controllerAs: 'ctrl'
});

function detailController($scope, $http, $location, service, $sce, $timeout) {
    var vm = this;
    var changeEvent;

    vm.$onInit = function() {
        vm.emails = service.getDetails($location.search().threadId);
    }

    $scope.safeHtml = $sce.trustAsHtml;
    $scope.query = $location.search().query;

    $scope.captureEvent = function(e) {
        changeEvent = e;
    }

    $scope.keyChange = function(e, subject) {
        if (e.keyCode == 13) {
            $scope.active = true;

            $http({
                url: '/sendSms',
                method: 'POST',
                data: {
                    subject: subject,
                    to: e.target.value
                }
            }).then(function(response) {
                $scope.active = false;
                $scope.smsResponse = response.status;

                $(e.target).val('');

                setTimeout(function() {
                    $('.mini.modal').modal('show');
                    $('.mini.modal').modal('refresh');
                }, 100);
            }, function(response) {
                if (response.status == 401)
                    $location.path('/');

                else {
                    $scope.active = false;
                    $scope.smsResponse = response.status;

                    setTimeout(function() {
                        $('.mini.modal').modal('show');
                        $('.mini.modal').modal('refresh');
                    }, 100);
                }
            });
        }
    }
}
