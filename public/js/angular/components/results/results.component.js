var app = angular.module('app');

app.component('results', {
    templateUrl: 'js/angular/components/results/results.template.html',
    controller: resultsController,
    controllerAs: 'ctrl'
});

function resultsController($scope, $http, $location, service) {
    var vm = this;

    vm.$onInit = function() {
        vm.results = service.getSearchResults($location.search().query);

        if(!vm.results)
            vm.results = new Array();

        vm.query = $location.search().query;
    }

    $scope.getDetails = function (email) {
        $scope.active = true;

        $http({
            url: 'details',
            method: 'GET',
            params : { threadId : email.threadId }
        }).then(function(response) {
            $scope.active = false;

            service.setDetails(email.threadId, response.data);
            $location.path('/details').search('threadId', email.threadId);
        }, function(response) {
            $scope.active = false;
            console.log(response);

            if (response.status == 401)
                $location.path('/');

            else
                $scope.response = 'There has been an error while fetching the details for thread Id ' + email.threadId;
        });
    }

    function handleError(response, $location) {
        if(response.status == 401)
            $location.path('/');

        else
            console.log(response);
    }
}
