app.service('service', function() {
    var cache = {};

    this.storeSearchResults = function (query, searchResults) {
        cache[query] = searchResults;
    }

    this.getSearchResults = function (query) {
        return cache[query];
    }

    this.setDetails = function (query, searchResults) {
        cache[query] = searchResults;
    }

    this.getDetails = function (query) {
        return cache[query];
    }
});
