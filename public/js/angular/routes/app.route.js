var app = angular.module('app');

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        template : '<login></login>'
    });

    $routeProvider.when('/results', {
        template : '<results></results>'
    });

    $routeProvider.when('/search', {
        template : '<search></search>'
    });

    $routeProvider.when('/details', {
        template : '<detail></detail>'
    });
});
